#include "calculateur_test.h"

void CalculateurTest::setUp() {
		this->objet_a_tester = new Calculateur();
}

void CalculateurTest::tearDown() {
		delete this->objet_a_tester;
}

// Correspond � d1 = <{0, 12}, {0}>
void CalculateurTest::test_propre() {
	int resultat = this->objet_a_tester->calculer(0, 12);
	CPPUNIT_ASSERT_EQUAL(0, resultat);
}



